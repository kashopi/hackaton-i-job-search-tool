#!/usr/bin/env python
# -*- coding: utf-8 -*-

from couchdbkit import *
from couchdbkit.designer import push

server = Server()
db = server.get_or_create_db('jobsbytechnology')
push('./jobsbytechnology', db)
db = server.get_or_create_db('employmentbycities')
push('./employmentbycities', db)