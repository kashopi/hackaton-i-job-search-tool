function(keys, values, rereduce){
	var min, city;
	for (i=0; i<values.length; ++i) {
	    var value = values[i];
	    if( value.ratio< min){
	    	min = value.ratio;
	    	city = value.city;
	    }
	}
	return {'ratio': min, 'city': city};
}