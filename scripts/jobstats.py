#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
from city_tools import *
import epa_ine

class Jobstats(object):
	def __init__(self):
		self.countries = {'Spain': {}}
		self.headers = {'Accept': 'text/plain',
						'Accept-Charset': 'utf-8',
						'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0'
					   } 

	def getURLpages(self, url):
		data = {}
		print "[URL] %s ..." % (url),
		r = requests.get(url, headers=self.headers)
		print " done!"
		
		if r.status_code == 200:
			data['DATA'] = r.text
		else:
			data['ERROR_TEXT']= 'Error while fetching %s with HTTP status code: %s' % (url,str(r.status_code))
			data['ERROR_CODE']= 'HTTP ' + str(r.status_code)

		return data


class Tecnoempleo(Jobstats):
	default_route = 'http://www.tecnoempleo.com/'
	def fetch(self, route=default_route):
		data = super(Tecnoempleo,self).getURLpages(route)
		if not data.has_key('ERROR_CODE'):
			return data['DATA']
		else:
			return ''

	def tecnologies_index(self):
		text = self.fetch()
		pattern = 'class="a_enlace_subrayado">([^\<]+)</a></td>[^\<]*<td class="tecnologias_cantidad" >([0-9]+)</td>'
		cpattern = re.compile(pattern)
		techs = []
		for m in cpattern.finditer(text):
			techs.append([m.group(1), int(m.group(2))])

		return techs

	def places_index(self):
		text = self.fetch()
		pattern = '<td class="tecnologias_nombre" style="vertical-align:top">[^\<]*<a title="Ofertas de empleo en [^\"]+" href="([^\"]+)" class="a_enlace_subrayado">([^\<]+)</a>'
		cpattern = re.compile(pattern)
		places = {}
		for m in cpattern.finditer(text):
			places[m.group(2)] = m.group(1)

		pattern = 'Total[^\<]+<b>([0-9]+)</b>'
		cities_names = places.keys()
		cities = []
		for city in cities_names:
			city_normalized = normalize_city(city)

			url = places[city]
			text = self.fetch(url)
			result = re.search(pattern, text)
			if result == None:
				jobs = 0
			else:
				jobs = result.group(1)

			cities.append([city_normalized, int(epa_ine.datos_EPA[city_normalized]*1000), int(jobs)])

		return cities


