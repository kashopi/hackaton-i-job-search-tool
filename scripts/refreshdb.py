#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../site/")

import datetime
import jobstats
from couchdbkit import *
from objects import *

#Connecting to database & init data
server = Server()
db = server.get_or_create_db('jobsbytechnology')
JobsByTechnology.set_db(db)

#Obtain jobs by technology ...
tecno = jobstats.Tecnoempleo()
techjobs = tecno.tecnologies_index()
#... and store them in database
for tech,jobs in techjobs:
	job = JobsByTechnology(
			technology = tech,
			jobs = jobs,
			date = datetime.datetime.utcnow()
		   )
	job.save()

#Now time for cities...
db = server.get_or_create_db('employmentbycities')
CityEmployment.set_db(db)

#Obtain city employment ...
cityjobs = tecno.places_index()
#... and store them in database
for city,nonworking,jobs in cityjobs:
	city = CityEmployment(
			city = city,
			nonworking = nonworking,
			jobs = jobs,
			date = datetime.datetime.utcnow()
		   )
	print city
	city.save()

