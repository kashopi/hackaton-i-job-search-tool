from pdfminer.pdfparser import PDFParser, PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice
from pdfminer.layout import LAParams, LTTextBoxHorizontal
from pdfminer.converter import PDFPageAggregator

fp = open('epapro0212.pdf','rb')
parser = PDFParser(fp)
doc = PDFDocument()

parser.set_document(doc)
doc.set_parser(parser)
doc.initialize()

if not doc.is_extractable:
	raise PDFTextExtractionNotAllowed

rsrcmgr = PDFResourceManager()
laparams = LAParams()
device = PDFPageAggregator(rsrcmgr, laparams=laparams)

provincias=[
u'Almer\xeda',
u'C\xe1diz',
u'C\xf3rdoba',
u'Granada',
u'Huelva',
u'Ja\xe9n',
u'M\xe1laga',
u'Sevilla',
u'Huesca',
u'Teruel',
u'Zaragoza',
u'Asturias, Principado de',
u'Balears, Illes',
u'Palmas, Las',
u'S.C.Tenerife',
u'Cantabria',
u'\xc1vila',
u'Burgos',
u'Le\xf3n',
u'Palencia',
u'Salamanca',
u'Segovia',
u'Soria ',
u'Valladolid',
u'Zamora',
u'Albacete',
u'Ciudad Real',
u'Cuenca',
u'Guadalajara',
u'Toledo',
u'Barcelona',
u'Girona',
u'Lleida',
u'Tarragona',
u'Alicante/Alacant',
u'Castell\xf3n/Castell\xf3',
u'Valencia/Val\xe8ncia',
u'Badajoz',
u'C\xe1ceres',
u'Coru\xf1a, A',
u'Lugo',
u'Ourense',
u'Pontevedra',
u'Madrid, Comunidad de',
u'Murcia, Regi\xf3n de',
u'Navarra, Comunidad Foral ',
u'Araba/\xc1lava',
u'Bizkaia',
u'Gipuzkoa',
u'Rioja, La',
u'Ceuta ',
u'Melilla']

datos=dict()
for provincia in provincias:
	interpreter = PDFPageInterpreter(rsrcmgr, device)
	for page in doc.get_pages():
		dic = dict()
		interpreter.process_page(page)
		layout = device.get_result()
		for lp in layout:
		    if isinstance(lp, LTTextBoxHorizontal):
			for lh in lp:
			    if lh.get_text().replace('\n','') == provincia:
				y0 = lh.y0
		layout = device.get_result()
		for ld in layout:
		    if isinstance(ld, LTTextBoxHorizontal):
			for lh in ld:
			    if lh.y0 < y0 + 1 and lh.y0 > y0: 
				dic[lh.get_text().replace('\n','')] = str(lh.x1)
		campos = sorted(dic.items(), key=lambda x: x[1])
		if len(campos) != 0:
			d=[]
			for campo in campos:
				d.append(campo[0])
			if provincia in datos:
				datos[provincia] = datos[provincia] + d
			else:
				datos[provincia] = d

data={}
provs = datos.keys()
for provincia in provs:
	data[provincia]=float(datos[provincia][1])

print data
'''for i in datos.keys():
	cadena = i
	for a in datos[i]:
		cadena = cadena + ';%f' % float(a)
	print (cadena)'''
