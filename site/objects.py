#!/usr/bin/env python
# -*- coding: utf-8 -*-
from couchdbkit import *

class CityEmployment(Document):
	city = StringProperty()
	nonworking = IntegerProperty()
	jobs = IntegerProperty()
	date = DateTimeProperty()


class JobsByTechnology(Document):
	technology = StringProperty()
	jobs = IntegerProperty()
	date = DateTimeProperty()
