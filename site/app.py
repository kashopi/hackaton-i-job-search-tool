#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template
from couchdbkit import *
from objects import *

app = Flask(__name__)
app.debug = True

@app.route('/')
def index():
	server = Server()
	db = server.get_or_create_db('jobsbytechnology')
	JobsByTechnology.set_db(db)
	techs = JobsByTechnology.view('jobsbytechnology/all', descending=True)
	return render_template('index.html', technologies = techs)

@app.route('/mejor-provincia-trabajo/', methods=['GET'])
def cityjobs():
	server = Server()
	db = server.get_or_create_db('employmentbycities')
	CityEmployment.set_db(db)
	cities = CityEmployment.view('employmentbycities/all', descending=True)
	return render_template('jobs-city.html', cities = cities)

@app.route('/provincia/<provincia>', methods=['GET'])
def show_test(provincia):
	return 'Not found'

if __name__=='__main__':
	app.run(host='0.0.0.0')

